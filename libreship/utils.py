#!/usr/bin/env python
# -*- coding: utf-8 -*-

import configparser
import datetime
import getpass
import logging
import os
import sys
import tempfile
from argparse import ArgumentParser
import keyring
from dateutil import parser, tz
from keyring.backends.file import EncryptedKeyring, PlaintextKeyring
from PIL import Image

logger = logging.getLogger(__name__)

try:
    input = raw_input
except NameError as e:
    pass

DEFAULT_PATH = os.path.join(os.path.dirname(__file__), 'config.ini')

def parse_arguments():
    # Parse arguments.
    parser = ArgumentParser(
        description='Create receipts and shipping labels for LibreShop orders.')
    parser.add_argument( '-d', '--log', dest='loglevel', action='store',
        default='ERROR', help=(
            'set log level [DEBUG, INFO, WARNING, ERROR, CRITICAL] '
            '(default: ERROR)'))
    parser.add_argument( '--debug', dest='debug', action='store_true',
        default=False, help='run in DEBUG mode')

    subparsers = parser.add_subparsers(title='subcommands', description='Valid subcommands',
                                       help='Select a command to run.', dest='command')
    subparsers.required = True

    parser_receipts = subparsers.add_parser(
        'receipts', help='Print receipts for new orders')
    parser_receipts.set_defaults(target='receipts')

    parser_labels = subparsers.add_parser('labels', help='Print shipping labels')
    parser_labels.set_defaults(target='labels')

    args = parser.parse_args()

    if args.debug:
        args.loglevel = 'DEBUG'

    # Configure logger.
    numeric_level = getattr(logging, args.loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % args.loglevel.upper())
    logging.basicConfig(level=numeric_level)

    return args


def get_config_path():

    config_path = None
    if os.environ.get('LIBRESHIP_CONFIG'):
        config_path = os.path.expanduser(os.environ.get('LIBRESHIP_CONFIG'))
    elif os.path.exists(DEFAULT_PATH):
        config_path = DEFAULT_PATH
    elif os.environ.get('XDG_CONFIG_HOME') and os.path.exists(
        os.path.join(os.environ.get('XDG_CONFIG_HOME'), 'libreship')):
        config_path = os.path.join(os.environ.get('XDG_CONFIG_HOME'), 'libreship')
    elif os.environ.get('HOME') and os.path.exists(
        os.path.join(os.environ.get('HOME'), '.config', 'libreship')):
        config_path = os.path.join(os.environ.get('HOME'), '.config', 'libreship')
    elif os.path.exists(os.path.expanduser('~/.libreship')):
        config_path = os.path.expanduser('~/.libreship')
    elif os.path.exists('/etc/libreship.cfg'):
        config_path = '/etc/libreship.cfg'

    return config_path


def update_config(section, values={}):

    result = False

    config_path = get_config_path()
    if config_path and values:
        logger.debug('Updating config.ini file.')
        config = configparser.ConfigParser()
        config.read(config_path)
        config[section].update({
            key:str(value) for key, value in values.items()
        })

        with open(config_path, 'w') as configfile:
            config.write(configfile)
        logger.debug('Updated config.ini file.')
        result = True

    return result


def check_config():

    config_path = get_config_path()

    if isinstance(keyring.get_keyring(), EncryptedKeyring):
        keyring.set_keyring(PlaintextKeyring())

    config = configparser.ConfigParser()
    domain, username, password, last_connected = None, None, None, None
    if config_path:
        try:
            logger.debug('Reading configuration file (%s).' % config_path)
            config.read(config_path)
        except configparser.MissingSectionHeaderError as e:
            logger.error('Could not read configuration file (%s).' %
                         config_path)
            sys.exit(str(e))
        else:
            domain = config['default']['domain'].strip()
            username = config[domain]['username'].strip()
            password = keyring.get_password(domain, username)
            logger.debug('Got credentials for %s@%s.' % (username, domain))

            last_connected = config[domain]['last_connected'].strip()
            last_connected = parser.parse(last_connected)
    else:
        logger.debug('No configuration file loaded.')

        domain = input('Enter the domain name (e.g.: www.example.com) > ')
        username = input('Enter your username > ')
        password = getpass.getpass('Enter your password (will not be echoed) > ')
        logger.debug('Got credentials for %s@%s.' % (username, domain))

        logger.debug('Saving password to keyring.')

        try:
            keyring.set_password(domain, username, password)
        except RuntimeError as e:
            sys.exit(str(e))
        except ValueError as e:
            print('%s.' % str(e), file=sys.stderr)
            keyring_dir = os.environ.get('XDG_DATA_HOME', None) or '~/.local/share'
            print("If you've forgotten the keyring password, remove the "
                  "\"%s/python_keyring\" directory and restart." % keyring_dir,
                  file=sys.stderr)
            sys.exit('Exiting...')
        logger.debug('Saved password to keyring.')

        # Set 'last connected' time to UNIX epoch.
        last_connected = datetime.datetime.fromtimestamp(0, tz=tz.tzlocal())

        logger.debug('Saving config.ini file.')
        config['default'] = {
            'domain': domain
        }
        config[domain] = {
            'username': username,
            'last_connected': last_connected
        }

        with open(DEFAULT_PATH, 'w') as configfile:
            config.write(configfile)
        logger.debug('Saved config.ini file.')

    return domain, username, password, last_connected


class ImageSplitter(object):
    MAX_IMAGE_WIDTH = 1755

    def __init__(self, image):
        self.image = Image.open(image)
        self._images = [] 
        self.split()

    def split(self):
        image_width, image_height = self.image.size

        self._images = list()
        if image_width > self.MAX_IMAGE_WIDTH:
            slices = int(math.ceil(image_width/self.MAX_IMAGE_WIDTH))
            for i in range(slices):
                right_bound = (
                    image_width if i+1 == slices
                    else (i+1) * self.MAX_IMAGE_WIDTH
                )
                bounding_box = (
                    i * self.MAX_IMAGE_WIDTH, # left bound
                    0,                        # upper bound
                    right_bound,              # right bound
                    image_height              # lower bound
                )
                sliced_image = self.image.crop(bounding_box)
                temp_file = tempfile.NamedTemporaryFile()
                sliced_image.save(temp_file, 'PNG')
                temp_file.flush()
                self._images.append(temp_file)
        else:
            temp_file = tempfile.NamedTemporaryFile()
            self.image.save(temp_file, 'PNG')
            temp_file.flush()
            self._images.append(temp_file)

    @property
    def images(self):
        return self._images

