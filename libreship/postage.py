import logging
import easypost

logger = logging.getLogger(__name__)

def create_address(address={}, verify=['delivery']):
    address_response = None
    try:
        street_address = address.get('street_address').split('\r\n', 1)
        address_response = easypost.Address.create(
            verify=verify,
            name = address.get('recipient_name'),
            street1 = street_address[0],
            street2 = (
                street_address[1] if len(street_address) > 1 else None
            ),
            city = address.get('locality'),
            state = address.get('region'),
            zip = address.get('postal_code'),
            country = address.get('country')
        )
    except easypost.Error as e:
        error = e.json_body.get('error')
        error_code = error.get('code')
        logger.error('%s error received from EasyPost API.' % error_code)
        if error_code == 'ADDRESS.VERIFY.INTL_NOT_ENABLED':
            address_response = create_address(address=address, verify=None)

    return address_response

