#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import math
import os
import signal
import sys
from collections import Counter
from io import BytesIO
from datetime import datetime, timezone
from decimal import Decimal
import cups
import easypost
import requests
from dateutil import parser
from itemize import Document
from silverscale.device import ScaleManager
from .utils import check_config, parse_arguments, ImageSplitter
from . import postage, gui, utils

logger = logging.getLogger(__name__)

easypost.api_key = os.environ.get('EASYPOST_API_KEY')

MINIMUM_PARCEL_WEIGHT = 0.1
OUNCES_PER_GRAM = 0.035274

def generate_receipts(order_list, last_connected):

    for order in order_list:
        order_time = parser.parse(order.get('created')).astimezone()
        purchases = order.get('purchases', [])

        manufactured_purchases = [
            purchase for purchase in purchases
            if not purchase.get('drop_shipped', False)
        ]

        if (order_time > last_connected) and manufactured_purchases:
            if purchases is not None:
                # Create an aggregate of all purchases within the order.
                purchases_aggregate = Counter(
                    purchase.get('name') for purchase in purchases)

                # Get the order's line items.
                line_items = list()
                for item_name, quantity in purchases_aggregate.items():
                    purchase = next(
                        purchase for purchase in purchases
                        if purchase.get('name') == item_name
                    )
                    line_items.append({
                        'sku': purchase.get('sku'),
                        'description': item_name,
                        'quantity': quantity,
                        'total': Decimal(purchase.get('price', 0.00)) * quantity
                    })

                shipping_address = order.get('shipping_address')
                recipient = shipping_address.get('recipient_name')
                street_address = shipping_address.get(
                    'street_address', '').replace('\r\n', ' ')
                locality = shipping_address.get('locality')
                region = shipping_address.get('region')
                postal_code = shipping_address.get('postal_code')
                country = shipping_address.get('country')
                country = country if country != 'US' else None

                address = [
                    recipient, street_address, locality, region, postal_code,
                    country
                ]

                receipt = Document(
                    template_name='receipt', data={
                    'order_token': order.get('token'),
                    'shipping_address': ', '.join([line for line in address if line]),
                    'line_items': line_items,
                    'split_shipment': any(
                        purchase.get('drop_shipped', False) for purchase in purchases
                    ),
                    'subtotal': order.get('subtotal', '0.00'),
                    'sales_tax': order.get('sales_tax', '0.00'),
                    'shipping_cost': order.get('shipping_cost', '0.00'),
                    'total': order.get('total', '0.00'),
                    'payment_method': order.get('payment_method', 'generic'),
                    'last_4': order.get('last_4'),
                    'created': '{time:%b} {time.day}, {time.year}'.format(
                        time=order_time)
                })
                receipt.print()


def check_server():
    domain, username, password, last_connected = utils.check_config()
    endpoint = 'http://%s/api/orders' % domain
    response = requests.get(endpoint, auth=(username, password))

    utc_time = datetime.now(timezone.utc) # UTC time
    current_time = utc_time.astimezone()  # Local time

    orders_response = response.json()
    next = orders_response.get('next')

    generate_receipts(orders_response.get('results', []), last_connected)

    while next:
        response = requests.get(next, auth=(username, password))
        orders_response = response.json()
        next = orders_response.get('next')

        generate_receipts(orders_response.get('results', []), last_connected)

    utils.update_config(domain, values={'last_connected': current_time})


def main_loop():

    domain, username, password, _ = utils.check_config()

    scale_manager = ScaleManager()
    scale = scale_manager.devices[0] if scale_manager.devices else None
    if scale is None:
        sys.exit('No USB scale is connected!')
    else:
        scale.connect()

    while True:
        order_id = input('Enter an order number: ')

        scale.read()
        weight = max(
            MINIMUM_PARCEL_WEIGHT,
            scale.weight if scale.units == 'oz' else (scale.weight *
                                                      OUNCES_PER_GRAM))
        print('Parcel weighs %f ounces.' % weight)

        if order_id:
            endpoint = 'http://%s/api/orders/%s' % (domain, order_id)
            response = requests.get(endpoint, auth=(username, password))

            if not response.ok:
                if response.status_code == 404:
                    print("Order '%s' not found!" % order_id)
                else:
                    print('Could not authenticate to website (%s).' % response.reason)
            else:
                print("Order '%s' found!" % order_id)
                order = response.json()

                from_address = easypost.Address.create(
                    verify=['delivery'],
                    company = 'LibreTees, LLC',
                    street1 = '2111 Jeff Davis Hwy',
                    street2 = 'Apt 405S',
                    city = 'Arlington',
                    state = 'VA',
                    zip = '22202',
                    country = 'US',
                    phone = '775-266-8727'
                )

                to_address = postage.create_address(order['shipping_address'])

                purchases = order.get('purchases', [])
                manufactured_purchases = [
                    purchase for purchase in purchases
                    if not purchase.get('drop_shipped', False)
                ]

                customs_info = None
                if purchases and to_address.country != from_address.country:
                    print('Creating customs form...')
                    purchases_aggregate = Counter(
                        purchase.get('name') for purchase in manufactured_purchases
                        if not purchase.get('fulfilled', True)
                    )

                    customs_items = list()
                    for item_name, quantity in purchases_aggregate.items():
                        purchase = next(
                            purchase for purchase in purchases
                            if purchase.get('name') == item_name
                        )

                        customs_item = easypost.CustomsItem.create(
                            description=purchase.get('name'),
                            quantity=quantity,
                            value=float(purchase.get('price', 0)) * quantity,
                            weight=max(
                                MINIMUM_PARCEL_WEIGHT,
                                float(purchase['weight']) * quantity *
                                OUNCES_PER_GRAM * 0.001
                            ),
                            origin_country='US'
                        )
                        customs_items.append(customs_item)

                    customs_info = easypost.CustomsInfo.create(
                        customs_certify=True,
                        customs_signer='Jared Contrascere',
                        contents_type='merchandise',
                        restriction_type='none',
                        restriction_comments='',
                        customs_items=customs_items
                    )

                parcel = easypost.Parcel.create(
                    predefined_package='Parcel',
                    weight=weight
                )
                shipment = easypost.Shipment.create(
                    to_address = to_address,
                    from_address = from_address,
                    parcel = parcel,
                    customs_info = customs_info
                )
                shipment.buy(rate = shipment.lowest_rate())

                label_response = requests.get(shipment.postage_label.label_url)
                label = BytesIO()
                label.write(label_response.content)

                labels = utils.ImageSplitter(label).images

                if label_response.ok:
                    print('Printing label...')
                    connection = cups.Connection()
                    printers = connection.getPrinters()
                    printer = [printer for printer in printers][0]

                    if 'paused' in printers[printer]['printer-state-reasons']:
                        print('Enabling printer...')
                        connection.enablePrinter(printer)

                    connection.printFiles(
                        printer,
                        [label.name for label in labels],
                        'Label',
                        {}
                    )

                    endpoint = 'https://%s/api/shipments' % domain
                    response = requests.post(endpoint, auth=(username, password), json={
                        'order_token': order_id,
                        'carrier': shipment.selected_rate.carrier,
                        'tracking_id': shipment.tracker.tracking_code,
                        'shipping_cost': shipment.selected_rate.rate,
                        'weight': '%d oz' % weight
                    })

                for purchase in manufactured_purchases:
                     endpoint = purchase.get('url')
                     if endpoint:
                         response = requests.patch(
                             endpoint, json={'fulfilled': True}, auth=(username, password))


def sigint_handler(signal, frame):
    scale_manager = ScaleManager()
    scale_manager.close()
    sys.exit('\nExiting...')


def main():
    signal.signal(signal.SIGINT, sigint_handler)

    args = utils.parse_arguments()
    logger.debug('Parsed arguments: %s' % args)

    display_available = True
    try:
        root = gui.Tk()
    except gui.TclError as e:
        logger.info('No display available. Entering CLI mode...')
        display_available = False
    else:
        logger.info('Display available. Entering GUI mode...')
        root.withdraw()
        app = gui.Application(master=root)
        root.mainloop()

    if not display_available:
        if args.target == 'labels':
            main_loop()
        elif args.target == 'receipts':
            check_server()


if __name__ == '__main__':
    main()
