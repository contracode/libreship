#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
try:
    # Import Tk for Python2
    from Tkinter import *
    import ttk
except ImportError as e:
    pass
try:
    # Import Tk for Python3
    from tkinter import *
    from tkinter import ttk
except ImportError as e:
    sys.exit("Tk package not found! Please install the '%s' package." %
        ('python-tk' if sys.version_info < (3, 0) else 'python3-tk')
    )

logger = logging.getLogger(__name__)


class LoginDialog(Toplevel):
    def __init__(self, master=None):
        Toplevel.__init__(self, master)

        self.title('Login')

        self.username_label = Label(self, text='Username')
        self.password_label = Label(self, text='Password')

        self.username_input = Entry(self)
        self.password_input = Entry(self, show='*')

        self.username_label.grid(row=0, sticky=E)
        self.password_label.grid(row=1, sticky=E)
        self.username_input.grid(row=0, column=1)
        self.password_input.grid(row=1, column=1)

        self.login_button = Button(self, text='Log in',
                                    command=self._login_button_clicked)
        self.login_button.grid(columnspan=2)

    def _login_button_clicked(self):
        username = self.username_input.get()
        password = self.password_input.get()

        showerror('Login Error', 'Invalid Username or Password!')


class Application(ttk.Frame):

    def __init__(self, *args, **kwargs):
        ttk.Frame.__init__(self, *args, **kwargs)
        self.master = kwargs.pop('master')
        self.width = 300
        self.height = 100
        self._init_window()


    def _init_menus(self):
        menu_bar = Menu(self.master)
        self.master.config(menu=menu_bar)

        file_menu = Menu(menu_bar, tearoff=0)
        file_menu.add_command(label='Connect', command=self.show_login_dialog )

        menu_bar.add_cascade(label='File', menu=file_menu)


    def _init_window(self):
        # Set the title of the master widget.
        self.master.title('LibreShip')
        self._init_menus()

        self.QUIT = Button(self, text="QUIT", fg="red",
                              command=self.master.destroy)
        self.QUIT.pack(side="bottom")

        self.pack()

        screen_width = self.winfo_screenwidth()
        screen_height = self.winfo_screenheight()

        # Determine required dimensions.
        self.master.update()
        required_width = self.master.winfo_reqwidth()
        required_height = self.master.winfo_reqheight()

        # Determine appropriate window dimensions based on screen size.
        window_width = (
            self.width if self.width < screen_width else required_width
        )
        window_height = (
            self.height if self.height < screen_height else required_height
        )

        # Determine window position.
        x_coord = screen_width/2 - window_width/2
        y_coord = screen_height/2 - window_height/2

        # Set window dimensions.
        self.master.geometry('%dx%d+%d+%d' % (window_width, window_height, x_coord, y_coord))

        # Draw Frame.
        self.master.deiconify()


    def show_login_dialog(self):
        self.login_dialog = LoginDialog(master=self)

    def say_hi(self):
        print("hi there, everyone!")

