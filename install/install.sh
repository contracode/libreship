#!/bin/sh
#########################################################
# Installer for udev usb rules for LibreShip USB Scales
#########################################################

# check if root
if [ $(id -u) -ne 0 ]; then
    # escalate
    echo "\nInstaller requires root, escalating via sudo."
    sudo "$0" "$@"
    exit $?
fi

echo "Linux installer script for LibreShip\n"

CURRENT_PATH=`pwd`
cd `dirname $0`
# save installer script location
SDK_PATH=`pwd`
# return to original path
cd $CURRENT_PATH

# if not OSX, install UDEV usb rules
if [ "`uname -s`" != "Darwin" ]; then
    echo "Installing rules for LibreShip USB Scales into /etc/udev/rules.d/"

    # Install UDEV rules for USB device
    if [ -f ${SDK_PATH}/libreship-usb.rules ]; then
        cp ${SDK_PATH}/libreship-usb.rules /etc/udev/rules.d/50-libreship-usb.rules
	udevadm trigger --attr-match=subsystem=usb
    else
        echo "ERROR: libreship-usb.rules file not found."
    fi
fi

echo "Done.\n"
